package threads.server;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingPeriodicWorkPolicy;

import com.google.android.material.color.DynamicColors;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.luminis.quic.QuicConnection;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.pages.PAGES;
import threads.server.core.pages.Page;
import threads.server.work.CleanupPeriodicWorker;
import threads.server.work.PagePeriodicWorker;

public class InitApplication extends Application {

    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String DAEMON_CHANNEL_ID = "DAEMON_CHANNEL_ID";
    public static final String DAEMON_GROUP_ID = "DAEMON_GROUP_ID";
    public static final String STORAGE_GROUP_ID = "STORAGE_GROUP_ID";
    private static final String TAG = InitApplication.class.getSimpleName();
    private final Gson gson = new Gson();


    private void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannelGroup(
                    new NotificationChannelGroup(STORAGE_GROUP_ID, name));

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void createDaemonChannel(@NonNull Context context) {


        try {
            CharSequence name = context.getString(R.string.daemon_channel_name);
            String description = context.getString(R.string.daemon_channel_description);
            NotificationChannel mChannel = new NotificationChannel(DAEMON_CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);


            notificationManager.createNotificationChannelGroup(
                    new NotificationChannelGroup(DAEMON_GROUP_ID, name));

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        createStorageChannel(getApplicationContext());
        createDaemonChannel(getApplicationContext());

        CleanupPeriodicWorker.cleanup(getApplicationContext());
        PagePeriodicWorker.publish(getApplicationContext(),
                ExistingPeriodicWorkPolicy.KEEP);

        long time = System.currentTimeMillis();
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.relays();
            ipfs.setPusher(this::onMessageReceived);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "finish start daemon ... " +
                    (System.currentTimeMillis() - time));
        }


    }

    public void onMessageReceived(@NonNull QuicConnection conn, @NonNull String content) {

        try {
            Type hashMap = new TypeToken<HashMap<String, String>>() {
            }.getType();

            Objects.requireNonNull(conn);
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            Objects.requireNonNull(content);
            Map<String, String> data = gson.fromJson(content, hashMap);

            LogUtils.error(TAG, "Push Message : " + data.toString());

            String ipns = data.get(Content.IPNS);
            Objects.requireNonNull(ipns);
            String pid = data.get(Content.PID);
            Objects.requireNonNull(pid);
            String seq = data.get(Content.SEQ);
            Objects.requireNonNull(seq);

            PeerId peerId = PeerId.fromBase58(pid);
            long sequence = Long.parseLong(seq);
            if (sequence >= 0) {
                if (ipfs.isValidCID(ipns)) {
                    PAGES pages = PAGES.getInstance(getApplicationContext());
                    Page page = pages.createPage(peerId.toBase58());
                    page.setContent(ipns);
                    page.setSequence(sequence);
                    pages.storePage(page);
                }
            }

            DOCS.getInstance(getApplicationContext()).addResolves(peerId, ipns);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
