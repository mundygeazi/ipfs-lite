package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import threads.lite.LogUtils;
import threads.server.Settings;
import threads.server.core.DOCS;

public class PageRefreshWorker extends Worker {

    private static final String TAG = PageRefreshWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PageRefreshWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {
        return new OneTimeWorkRequest.Builder(PageRefreshWorker.class)
                .addTag(TAG)
                .build();
    }

    public static void publish(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, "Start " + getId().toString() + " ...");

        try {
            int seq = Settings.getSequence(getApplicationContext());
            seq++;
            Settings.setSequence(getApplicationContext(), seq);
            DOCS docs = DOCS.getInstance(getApplicationContext());
            docs.publishPage(this::isStopped, seq);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, "Finish " + getId().toString() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


}

