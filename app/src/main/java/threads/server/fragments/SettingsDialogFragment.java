package threads.server.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.work.ExistingPeriodicWorkPolicy;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.slider.Slider;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.Objects;

import threads.lite.LogUtils;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.DOCS;
import threads.server.services.LiteService;
import threads.server.work.PagePeriodicWorker;

public class SettingsDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = SettingsDialogFragment.class.getSimpleName();

    private Context mContext;

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setContentView(R.layout.settings_view);

        SwitchMaterial enableRedirectUrl = dialog.findViewById(R.id.enable_redirect_url);
        Objects.requireNonNull(enableRedirectUrl);
        enableRedirectUrl.setChecked(Settings.isRedirectUrlEnabled(mContext));
        enableRedirectUrl.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    Settings.setRedirectUrlEnabled(mContext, isChecked);
                    DOCS.getInstance(mContext).refreshRedirectOptions(mContext);
                }
        );

        SwitchMaterial enableRedirectIndex = dialog.findViewById(R.id.enable_redirect_index);
        Objects.requireNonNull(enableRedirectIndex);
        enableRedirectIndex.setChecked(Settings.isRedirectIndexEnabled(mContext));
        enableRedirectIndex.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    Settings.setRedirectIndexEnabled(mContext, isChecked);
                    DOCS.getInstance(mContext).refreshRedirectOptions(mContext);
                }
        );


        SwitchMaterial enableJavascript = dialog.findViewById(R.id.enable_javascript);
        Objects.requireNonNull(enableJavascript);
        enableJavascript.setChecked(Settings.isJavascriptEnabled(mContext));
        enableJavascript.setOnCheckedChangeListener((buttonView, isChecked) ->
                Settings.setJavascriptEnabled(mContext, isChecked)
        );


        TextView publisher_service_time_text = dialog.findViewById(R.id.publisher_service_time_text);
        Objects.requireNonNull(publisher_service_time_text);
        Slider publisher_service_time = dialog.findViewById(R.id.publisher_service_time);
        Objects.requireNonNull(publisher_service_time);


        publisher_service_time.setValueFrom(2);
        publisher_service_time.setValueTo(12);
        int time = 0;
        int pinServiceTime = LiteService.getPublishServiceTime(mContext);
        if (pinServiceTime > 0) {
            time = (pinServiceTime);
        }
        publisher_service_time_text.setText(getString(R.string.publisher_service_time,
                String.valueOf(time)));
        publisher_service_time.setValue(time);
        publisher_service_time.addOnChangeListener((slider, value, fromUser) -> {

            try {
                int data = (int) value;
                LiteService.setPublisherServiceTime(mContext, data);

                publisher_service_time_text.setText(
                        getString(R.string.publisher_service_time,
                                String.valueOf(data)));

                PagePeriodicWorker.publish(mContext, ExistingPeriodicWorkPolicy.REPLACE);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        publisher_service_time.setEnabled(true);
        publisher_service_time_text.setEnabled(true);

        return dialog;
    }

}
